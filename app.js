const boxes = document.querySelectorAll(".box");
const form = document.getElementById("form");
const submitForm = document.getElementById("form-submit");

submitForm.addEventListener("click", (e) => {
  e.preventDefault();
  const name = form.name.value;
  const experience = form.radio.value;
  const city = form.select.value;
  const languages = document.getElementsByName("checkbox");
  let obj = {};
  let languagesArr = [];
  for (let val of languages) {
    if (val.checked === true) {
      languagesArr.push(val.value);
    }
  }
  obj.name = name;
  obj.experience = experience;
  obj.city = city;
  obj.languages = languagesArr;
  console.log(obj);
});

function attachCounter() {
  let count = 0;
  const counter = () => {
    count++;
  };
  return {
    counter,
    count: () => count,
  };
}

for (let i = 0; i < boxes.length; i++) {
  const box = boxes[i];
  const counter = attachCounter();
  box.addEventListener("mouseover", (event) => {
    counter.counter();
    event.target.innerText = `Hovered ${counter.count()} Time(s)`;
  });
}
